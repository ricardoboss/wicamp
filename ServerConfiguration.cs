﻿using System.IO;

namespace WiCaMP
{
    public class ServerConfiguration
    {
        private const string AutostartHeader = "Autostart";
        private const string ExeFileHeader = "Exe";
        private const string ArgumentsHeader = "Args";
        private const string WorkingDirectoryHeader = "WorkingDir";
        private const string KillCommandHeader = "KillCommand";

        private bool _autoStart;
        private FileInfo _exeFile;
        private string _arguments;
        private DirectoryInfo _workingDirectory;
        private string _killCommand;

        internal readonly ServerType Type;

        #region Properties
        
        internal bool AutoStart
        {
            get => _autoStart;
            set
            {
                _autoStart = value;
                
                Program.Config.Configs[AutostartHeader].Set(Type.Name, value);
            }
        }

        internal FileInfo ExeFile
        {
            get => _exeFile;
            set
            {
                _exeFile = value;
                
                Program.Config.Configs[ExeFileHeader].Set(Type.Name, value.ToString());
            }
        }

        internal string Arguments
        {
            get => _arguments;
            set
            {
                _arguments = value;
                
                Program.Config.Configs[ArgumentsHeader].Set(Type.Name, value);
            }
        }

        internal DirectoryInfo WorkingDirectory
        {
            get => _workingDirectory;
            set
            {
                _workingDirectory = value;
                
                Program.Config.Configs[WorkingDirectoryHeader].Set(Type.Name, value.ToString());
            }
        }

        internal string KillCommand
        {
            get => _killCommand;
            set
            {
                _killCommand = value;
                
                Program.Config.Configs[KillCommandHeader].Set(Type.Name, value);
            }
        }

        #endregion
        
        public ServerConfiguration(ServerType type)
        {
            Type = type;

            _autoStart = GetBool(AutostartHeader, Type.Autostart);

            var exeFilePath = Utils.EnsureAbsolutePath(GetString(ExeFileHeader, Type.ExeFilePath));
            var workingDirectoryPath = Utils.EnsureAbsolutePath(GetString(WorkingDirectoryHeader, Type.WorkingDirectoryPath));

            _exeFile = new FileInfo(exeFilePath);
            _workingDirectory = new DirectoryInfo(workingDirectoryPath);

            _arguments = GetString(ArgumentsHeader, Type.Arguments);
            _killCommand = GetString(KillCommandHeader, Type.KillCommand);
        }

        private bool GetBool(string header, bool defaultValue)
        {
            return Program.Config.Configs[header]?.GetBoolean(Type.Name, defaultValue) ?? defaultValue;
        }

        private string GetString(string header, string defaultValue)
        {
            return Program.Config.Configs[header]?.GetString(Type.Name, defaultValue) ?? defaultValue;
        }
    }
}