﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace WiCaMP
{
    internal static class ServerManager
    {
        public static event EventHandler<ProcessStartedEventArgs> ProcessStarted;
        public static event EventHandler<ProcessStoppedEventArgs> ProcessStopped;

        // timout in milliseconds
        private const int ProcessShutdownTimeout = 10000;
        
        private static readonly Dictionary<ServerType, Process> Processes = new Dictionary<ServerType, Process>()
        {
            { ServerType.Caddy,  new Process() },
            { ServerType.MySql,  new Process() },
            { ServerType.Php,  new Process() }
        };
        
        private static readonly Dictionary<ServerType, ServerConfiguration> Configurations = new Dictionary<ServerType, ServerConfiguration>();

        internal static void SetConfiguration(ServerConfiguration config)
        {
            Configurations[config.Type] = config;
            
            var info = new ProcessStartInfo()
            {
                Arguments = config.Arguments,
                FileName = config.ExeFile.ToString(),
                WorkingDirectory = config.WorkingDirectory.ToString(),
                UseShellExecute = false,
                RedirectStandardInput = true,
                RedirectStandardOutput = true
            };

            var process = Processes[config.Type];
            process.StartInfo = info;
            process.EnableRaisingEvents = true;
            
            // hook into the process exited event to catch when a process exits
            process.Exited += (sender, parentArgs) =>
            {
                var eventArgs = new ProcessStoppedEventArgs
                {
                    Type = config.Type
                };

                ProcessStopped?.Invoke(sender, eventArgs);
            };
        }

        internal static bool IsRunning(ServerType type)
        {
            var process = Processes[type];

            if (process == null)
                return false;
            
            try
            {
                Process.GetProcessById(process.Id);
            }
            catch (Exception)
            {
                return false;
            }

            return !process.HasExited;
        }

        internal static int GetPid(ServerType type)
        {
            if (!IsRunning(type))
                return -1;

            return Processes[type].Id;
        }
        
        internal static void StartAll(IEnumerable<ServerType> types)
        {
            foreach (var type in types)
            {
                Start(type);
            }
        }

        internal static void Start(ServerType type)
        {
            var process = Processes[type];

            if (process == null)
            {
                Program.Logger.ErrorFormat($"Process for {type.Name} server has not been created yet!");

                return;
            }
            
            var success = process.Start();

            if (success)
            {
                var args = new ProcessStartedEventArgs
                {
                    Type = type,
                    ProcessId = process.Id
                };

                ProcessStarted?.Invoke(null, args);

                return;   
            }
            
            Program.Logger.Error($"Process for {type.Name} server could not be started!");
        }

        internal static void StopAllRunning()
        {   
            foreach (var type in Processes.Keys)
            {
                if (IsRunning(type))
                    Stop(type);
            }
        }
        
        internal static void RestartAllRunning()
        {
            foreach (var type in Processes.Keys)
            {
                if (IsRunning(type))
                    Restart(type);
            }
        }

        internal static void Restart(ServerType type)
        {
            Stop(type);
            Start(type);
        }

        internal static void HookConsole(ServerType type)
        {
            var process = Processes[type];
            
            Program.Logger.Info($"Hooked into console for {type.Name} server.");
            Program.Logger.Info("To get back to the standard console, hit CTRL + B");
            Console.WriteLine();

            bool[] exit = {false};
            
            var outputThread = new Thread(() =>
            {
                while (!exit[0])
                {
                    Console.Out.Write((char) process.StandardOutput.Read());
                }
            });
            
            outputThread.Start();

            do
            {
                var cki = Console.ReadKey(true);

                if (cki.Key == ConsoleKey.B && cki.Modifiers.HasFlag(ConsoleModifiers.Control))
                    exit[0] = true;
                else
                {
                    Console.Write(cki.KeyChar);
                    process.StandardInput.Write(cki);                    
                }
            } while (!exit[0]);
            
            outputThread.Interrupt();
            
            Console.WriteLine();
            Program.Logger.Info("(Back to standard console)");
        }

        internal static void Stop(ServerType type)
        {
            var process = Processes[type];

            if (process == null)
            {
                Program.Logger.Error($"No process found for {type.Name} server!");

                return;
            }

            if (process.HasExited)
                return;

            // read kill command from config
            var killCommand = Configurations[type].KillCommand;

            // replace pid
            killCommand = killCommand.Replace("{PROCESS_ID}", process.Id.ToString());

            // create new process executing the kill command
            var info = new ProcessStartInfo
            {
                Arguments = $"/c \"{killCommand}\"",
                FileName = @"C:\Windows\System32\cmd.exe",
                CreateNoWindow = true,
                WindowStyle = ProcessWindowStyle.Hidden
            };
            
            Program.Logger.Debug($"Executing \"{info.FileName} {info.Arguments}\"");
            
            var killProcess = new Process
            {
                StartInfo = info,
                EnableRaisingEvents = true
            };
            
            killProcess.Start();

            Program.Logger.Debug($"Waiting for {type.Name} process to exit ({process.Id})");

            // wait for the process to exit
            var exited = process.WaitForExit(ProcessShutdownTimeout);
            
            // force-kill the process if it is still runnning
            if (!exited)
            {
                Program.Logger.Warn($"Process for {type.Name} server did not exit within the timeout!");
                Program.Logger.Warn($"Killing process {process.Id}...");
                
                process.Kill();
            }
            
            // end the kill process
            killProcess.WaitForExit();
            killProcess.Dispose();
        }

        internal static void Dispose()
        {
            foreach (var type in Processes.Keys)
            {
                if (IsRunning(type))
                    Stop(type);
                
                Processes[type].Dispose();
            }
        }
    }

    internal class ProcessStartedEventArgs : EventArgs
    {
        public ServerType Type { get; set; }
        public int ProcessId { get; set; }
    }

    internal class ProcessStoppedEventArgs : EventArgs
    {
        public ServerType Type { get; set; }
    }
}