﻿using System;
using System.Diagnostics;
using System.Reflection;

namespace WiCaMP
{
    internal static class Commands
    {
        private static readonly string Version = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).ProductVersion;

        internal static void PrintVersion()
        {
            Program.Logger.Info("WiCaMP Version " + Version);
        }
    }
}