﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using log4net;
using log4net.Config;
using Nini.Config;

namespace WiCaMP
{
    internal static partial class Program
    {
        internal const string Seperator = "======================================";
        private const string WicampHeader = "WiCaMP";
        
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(Program));
        internal static IConfigSource Config { get; private set; }
        
        private static readonly Dictionary<ServerType, ServerConfiguration> ServerConfigurations = new Dictionary<ServerType, ServerConfiguration>();

        public static void Main(string[] args)
        {
            var commandLineConfiguration = new CommandLineConfiguration();
            
            // configure log4net
            // BasicConfigurator is used if no log4net configuration file is found
            BasicConfigurator.Configure();
            
            // load log4net config file, if it exists
            var logConfigFilePath = Path.Combine(Environment.CurrentDirectory, "log4net.xml");
            var logConfigFile = new FileInfo(logConfigFilePath);

            if (logConfigFile.Exists)
            {
                XmlConfigurator.Configure(logConfigFile);
                
                Logger.DebugFormat("Using \"{0}\" as log4net configuration file.", logConfigFile);
            }
            else
                Logger.WarnFormat("No log4net.xml file found in \"{0}\"", Environment.CurrentDirectory);
            
            // parse command line arguments
            if (CommandLine.Parser.Default.ParseArguments(args, commandLineConfiguration))
            {
                Logger.Debug("Command line arguments parsed.");
                
                // print the version on program start
                Commands.PrintVersion();
            }
            else
            {
                Logger.Fatal("Error(s) while parsing command line arguments");
                Logger.Fatal("Program will exit.");

                Environment.Exit(1);
            }
            
            // check if config file exists
            var configFile = new FileInfo(Environment.CurrentDirectory + "\\" + commandLineConfiguration.ConfigFile);
            if (configFile.Exists)
            {
                Logger.DebugFormat("Using config file \"{0}\"", configFile);
            }
            else
            {
                Logger.FatalFormat("Specified config file could not be found (\"{0}\")", configFile);
                Logger.Fatal("Program will exit.");

                Environment.Exit(2);
            }

            // load config
            Config = new IniConfigSource(configFile.FullName) {AutoSave = true};

            // add save event handler
            Config.Saved += (sender, eventArgs) =>
            {
                Logger.Info("Configuration saved!");
            };

            // initialize program after logger and config have been loaded
            LoadConfigs();
            
            // read boolean from config
            var firstTime = Config.Configs[WicampHeader].GetBoolean("First Start", true);

            // if this is not the first time the server is started, go straight to startup
            if (!firstTime)
            {
                Startup();

                return;
            }

            Logger.Info("This is the first time WiCaMP is started.");

            // ask the user to enter y, n or press enter to determine whether to enter setup now
            var k = PromptKey("Do you want to setup the config now? (Y/n) ", true);

            // depending on the key the user pressed, execute setup or go to startup
            if (k == ConsoleKey.Y || k == ConsoleKey.Enter)
                Setup();
            else
            {
                Logger.Warn("Please take a look at the configuration file to ensure that the program will run as expected.");
                
                Startup();
            }
        }

        internal static void LoadConfigs()
        {
            // initialize server configurations
            foreach (var server in ServerType.Values)
            {
                ServerConfigurations[server] = new ServerConfiguration(server);
            }
        }
    }
}