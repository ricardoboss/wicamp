﻿using System;
using System.Linq;
using System.Threading;

namespace WiCaMP
{
    internal partial class Program
    {
        private static void Startup()
        {
            Logger.Info(Seperator);
            
            Logger.Debug("Auto-starting servers");

            ServerManager.ProcessStarted += ServerManagerOnProcessStarted;
            ServerManager.ProcessStopped += ServerManagerOnProcessStopped;
            
            // store server configurations in server manager
            foreach (var type in ServerConfigurations.Keys)
            {
                ServerManager.SetConfiguration(ServerConfigurations[type]);
            }
            
            // collect server that should be automatically started
            var autostartServers = ServerType.Values.Where(server => ServerConfigurations[server].AutoStart).ToList();
            
            // start the autostart servers
            ServerManager.StartAll(autostartServers);
            
            // run command parser in main thread
            CommandParser.Start();

            // stop all running servers
            Logger.Info("Stopping all running servers");
            ServerManager.StopAllRunning();
            
            ServerManager.Dispose();
            
            Console.WriteLine();
            Logger.Info("Bye!");
            
            // wait 3 seconds before exiting
            Thread.Sleep(3000);
            
            Console.WriteLine();
        }

        private static void ServerManagerOnProcessStopped(object sender, ProcessStoppedEventArgs processStoppedEventArgs)
        {
            Logger.Warn($"Process for server {processStoppedEventArgs.Type.Name} stopped!");
        }

        private static void ServerManagerOnProcessStarted(object sender, ProcessStartedEventArgs processStartedEventArgs)
        {
            Logger.Info($"Process {processStartedEventArgs.ProcessId} started for server {processStartedEventArgs.Type.Name}");
        }
    }
}