﻿using System;
using System.IO;
using System.Linq;

namespace WiCaMP
{
    internal static class Utils
    {
        internal static string EnsureAbsolutePath(string relativePath)
        {
            // check if absolute path is given; use CWD otherwise
            return relativePath == Path.GetFullPath(relativePath) ? relativePath : Path.Combine(Environment.CurrentDirectory, relativePath);
        }
    }
}