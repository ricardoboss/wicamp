﻿using System.Collections.Generic;
using System.Linq;

namespace WiCaMP
{
    public class ServerType
    {
        internal static readonly ServerType Caddy = new ServerType(
            false,
            "Caddy",
            @"caddy\caddy.exe",
            "-agree -http2",
            "www",
            "taskkill /F /PID {PROCESS_ID}"
        );
        
        internal static readonly ServerType MySql = new ServerType(
            false,
            "MySQL",
            @"mysql\bin\mysqld.exe",
            "",
            "mysql",
            @"mysql\bin\mysqladmin.exe -u root shutdown"
        );
        
        internal static readonly ServerType Php = new ServerType(
            false,
            "PHP",
            @"php\php-cgi.exe",
            "-b 9000",
            "php",
            "taskkill /F /PID {PROCESS_ID}"
        );

        internal static IEnumerable<ServerType> Values
        {
            get
            {
                yield return Caddy;
                yield return MySql;
                yield return Php;
            }
        }

        public static ServerType FromString(string name)
        {
            return Values.FirstOrDefault(type => type.Name.ToLower().Equals(name.ToLower()));
        }

        internal readonly bool Autostart;
        internal readonly string Name;
        internal readonly string ExeFilePath;
        internal readonly string Arguments;
        internal readonly string WorkingDirectoryPath;
        internal readonly string KillCommand;

        private ServerType(bool autostart, string name, string exeFilePath, string arguments, string workingDirectoryPath, string killCommand)
        {
            Autostart = autostart;
            Name = name;
            ExeFilePath = exeFilePath;
            Arguments = arguments;
            WorkingDirectoryPath = workingDirectoryPath;
            KillCommand = killCommand;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}