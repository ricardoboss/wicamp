﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using CommandLine;
using CommandLine.Text;

namespace WiCaMP
{
    public class CommandLineConfiguration
    {
        private const string ConfigFileName = "wicamp.xml";
        
        [Option(
            'c', 
            "config", 
            Required = true, 
            HelpText = "The configuration file to use."
            )]
        public string ConfigFile { get; set; }

        [ParserState]
        public IParserState LastParserState { get; set; }

        [HelpOption]
        public string GetUsage()
        {
            return HelpText.AutoBuild(
                this,
                (current) => HelpText.DefaultParsingErrorsHandler(this, current)
            );
        }
    }
}