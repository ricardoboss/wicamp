﻿using System;
using System.IO;

namespace WiCaMP
{
    internal partial class Program
    {
        private static void Setup()
        {
            Logger.Info("Entering Setup");

            // turn off autosave for now; will be turned on later
            Config.AutoSave = false;

            // get enumerator for all server types
            var enumerator = ServerType.Values.GetEnumerator();

            // move to the first element
            enumerator.MoveNext();
            
            // loop through all elements
            ServerType type;
            while ((type = enumerator.Current) != null)
            {
                Logger.Info(Seperator);
                
                Logger.InfoFormat("Configuration for server \"{0}\"", type.Name);

                PromptAutostart(type);
                PromptExeFile(type);
                PromptArguments(type);
                PromptWorkingDirectory(type);
                PromptKillCommand(type);

                Logger.Info(Seperator);
                
                // display all information combined
                Logger.InfoFormat("Server will {0}start automatically.", ServerConfigurations[type].AutoStart ? "" : "NOT ");
                Logger.InfoFormat("Program arguments are: {0}", ServerConfigurations[type].Arguments);
                Logger.InfoFormat("Working directory is: {0}", ServerConfigurations[type].WorkingDirectory);
                Logger.InfoFormat("Kill command is: {0}", ServerConfigurations[type].KillCommand);

                // ask the user if the information is correct (y/n question)
                // jump back to the start of the loop, not altering the enumerator, if not
                if (PromptKey("Is this information correct? (y/n)") == ConsoleKey.N)
                    continue;
                
                // break out of the loop, if the last element of the list has been surpassed
                if (!enumerator.MoveNext())
                    break;
            }
            
            // dispose the enumerator
            enumerator.Dispose();
            
            // do not ask for setup next start
            Config.Configs[WicampHeader].Set("First Start", false);
            
            // save the configuration of all servers
            Config.Save();

            // turn auto save back on
            Config.AutoSave = true;
            
            Logger.Info("Entering Startup");
            
            // next up is the casual startup
            Startup();
        }

        private static ConsoleKey PromptKey(string text, bool hasDefault = false)
        {
            ConsoleKey key;

            do
            {
                Console.Write(text);
                
                key = Console.ReadKey().Key;
                
                Console.WriteLine();
            } while (
                (key != ConsoleKey.Y && key != ConsoleKey.N && key != ConsoleKey.Enter) ||
                (!hasDefault && key == ConsoleKey.Enter)
            );
            
            return key;
        }

        private static string PromptString(string text, string defaultValue)
        {
            Console.Write($"{text} ({defaultValue}): ");

            var line = Console.ReadLine();

            if (line != null)
                return string.IsNullOrWhiteSpace(line) ? defaultValue : line;
            
            Logger.Error("Please enter a valid string!");

            return null;
        }

        private static void PromptAutostart(ServerType type)
        {
            // whether or not to start the server automatically; default is yes
            var autoStart = PromptKey("Autostart? (y/n)") != ConsoleKey.N;
            
            // set autostart value for current server in config
            Logger.InfoFormat("{0} server will {1}start automatically.", type.Name, autoStart ? "" : "NOT ");
            ServerConfigurations[type].AutoStart = autoStart;
        }

        private static void PromptExeFile(ServerType type)
        {
            FileInfo exeFile;
            do
            {
                var exeFilePath = PromptString("Relative path to exe file",
                    ServerConfigurations[type].ExeFile.ToString());

                exeFilePath = Utils.EnsureAbsolutePath(exeFilePath);

                exeFile = new FileInfo(exeFilePath);

                if (!exeFile.Exists)
                    Logger.ErrorFormat("File \"{0}\" does not exist!", exeFile);
            } while (!exeFile.Exists);
            
            Logger.InfoFormat("Using \"{0}\" as the exe file.", exeFile);
            ServerConfigurations[type].ExeFile = exeFile;
        }
        
        private static void PromptKillCommand(ServerType type)
        {
            var killCommand = PromptString("Kill command",
                ServerConfigurations[type].KillCommand);

            if (string.IsNullOrWhiteSpace(killCommand))
                killCommand = "taskkill /PID {PROCESS_ID}";

            Logger.InfoFormat("Using \"{0}\" as kill command.", killCommand);
            ServerConfigurations[type].KillCommand = killCommand;
        }

        private static void PromptWorkingDirectory(ServerType type)
        {
            // loop until a given directory exists
            DirectoryInfo workingDirectory;
            do
            {
                var workingDirectoryPath = PromptString("Relative path to working directory",
                    ServerConfigurations[type].WorkingDirectory.ToString());
                    
                workingDirectoryPath = Utils.EnsureAbsolutePath(workingDirectoryPath);
                    
                workingDirectory = new DirectoryInfo(workingDirectoryPath);
                    
                if (!workingDirectory.Exists)
                    Logger.ErrorFormat("\"{0}\" does not exist!", workingDirectory);
            } while (!workingDirectory.Exists);

            // save working directory
            Logger.InfoFormat("Using \"{0}\" as the working directory.", workingDirectory);
            ServerConfigurations[type].WorkingDirectory = workingDirectory;
        }

        private static void PromptArguments(ServerType type)
        {
            // prompt arguments
            var arguments = PromptString("Program arguments; (space) to delete all",
                ServerConfigurations[type].Arguments);

            // " " (space) means no arguments at all
            if (arguments == " ")
                arguments = "";

            // save arguments in configuration
            Logger.InfoFormat("Using \"{0}\" as program arguments.", arguments);
            ServerConfigurations[type].Arguments = arguments;
        }
    }
}