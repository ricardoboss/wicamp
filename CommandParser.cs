﻿using System;
using System.Collections.Generic;

namespace WiCaMP
{
    internal static class CommandParser
    {
        public static void Start()
        {
            var logger = Program.Logger;
            var exit = false;

            string line;

            while ((line = Console.In.ReadLine()) != null)
            {
                var tokens = line.Trim().Split(' ');

                ServerType type;
                
                switch (tokens[0])
                {
                    case "console":
                        if (!CheckArguments(tokens, 2, 2, "console <server>"))
                            break;

                        type = ServerType.FromString(tokens[1]);

                        if (type == null)
                        {
                            logger.Error($"\"{tokens[1]}\" is not a valid server!");
                            logger.Error("Valid values for <server> are: caddy, mysql, php");

                            break;
                        }

                        if (!ServerManager.IsRunning(type))
                        {
                            logger.Error($"Server {type.Name} is not running!");
                            logger.Error($"Start it using the command \"start {type.Name}\".");

                            break;
                        }

                        ServerManager.HookConsole(type);
                        
                        break;
                        
                    case "start":
                        if (!CheckArguments(tokens, 2, 2, "start <server>"))
                            break;

                        type = ParseServer(tokens[1]);
                        
                        if (type == null)
                            break;

                        if (ServerManager.IsRunning(type))
                        {
                            logger.Error($"Server {type.Name} is already running!");

                            break;
                        }

                        logger.Info($"Starting server {type.Name}");
                        
                        ServerManager.Start(type);
                        
                        break;
                    
                    case "stop":
                        if (!CheckArguments(tokens, 2, 2, "stop <server>"))
                            break;

                        type = ParseServer(tokens[1], true);

                        if (type == null)
                        {
                            if (tokens[1] == "all")
                            {
                                logger.Info("Stopping all running servers");

                                ServerManager.StopAllRunning();
                            }

                            break;
                        }

                        if (!ServerManager.IsRunning(type))
                        {
                            logger.Error($"Cannot stop server {type.Name}: server is not running!");

                            break;
                        }
                        
                        logger.Info($"Stopping server {type.Name}");

                        ServerManager.Stop(type);
                        
                        break;
                    
                    case "restart":
                        if (!CheckArguments(tokens, 2, 2, "restart <server>"))
                            break;
                        
                        type = ParseServer(tokens[1], true);

                        if (type == null)
                        {
                            if (tokens[1] == "all")
                            {
                                logger.Info("Restarting all running servers");

                                ServerManager.RestartAllRunning();
                            }

                            break;
                        }

                        if (!ServerManager.IsRunning(type))
                        {
                            logger.Error($"Cannot restart server {type.Name}: server is not running!");

                            break;
                        }
                        
                        logger.Info($"Restarting server {type.Name}");

                        ServerManager.Restart(type);
                        
                        break;
                        
                    case "status":
                        logger.Info(Program.Seperator);
                        logger.Info("Status");
                        logger.Info(Program.Seperator);
                        
                        foreach (var serverType in ServerType.Values)
                        {
                            if (ServerManager.IsRunning(serverType))
                            {
                                var pid = ServerManager.GetPid(serverType);
                            
                                logger.Info($"Server {serverType.Name} is running with PID {pid}");
                            }
                            else
                            {
                                logger.Info($"Server {serverType.Name} is not running");
                            }
                        }
                        
                        logger.Info(Program.Seperator);
                        break;
                    
                    case "reload":
                        logger.Info("Reloading configurations");
                        
                        Program.LoadConfigs();
                        
                        break;
                    
                    case "exit":
                        exit = true;
                        break;

                    case "help":
                        logger.Info(Program.Seperator);
                        logger.Info("Help for WiCaMP commands");
                        logger.Info(Program.Seperator);
                        logger.Info("help - shows this help message");
                        logger.Info("console <server> - hook into the console for a server");
                        logger.Info("start <server> - start a server");
                        logger.Info("stop <server|all> - stop a/all server(s)");
                        logger.Info("restart <server|all> - restart a/all server(s)");
                        logger.Info("status - display information about the servers");
                        logger.Info("reload - reload configuration file");
                        logger.Info("exit - stop all running servers and exit");
                        logger.Info(Program.Seperator);
                        break;
                        
                    default:
                        logger.Error("command not found: " + tokens[0]);
                        logger.Info("Use \"help\" to show a list of possible commands.");
                        break;
                }

                if (exit)
                    break;
            }
        }

        private static bool CheckArguments(IReadOnlyCollection<string> tokens, int min, int max, string usage)
        {
            if (tokens.Count >= min && tokens.Count <= max)
                return true;
            
            Program.Logger.Error($"Usage: {usage}");

            return false;
        }

        private static ServerType ParseServer(string typeName, bool withAll = false)
        {
            var type = ServerType.FromString(typeName);

            if (type != null)
                return type;

            if (withAll && typeName == "all")
                return null;
            
            Program.Logger.Error($"\"{typeName}\" is not a valid server!");
            Program.Logger.Error("Valid values for <server> are: caddy, mysql, php");

            return null;
        }
    }
}